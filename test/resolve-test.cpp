#include "gtest/gtest.h"

#include "IUObject.h"
#include "resolve.h"

TEST(resolve, normal)
{
    UObject tank;
    int *number1_ptr = new int(100);
    int *number2_ptr = new int(200);

    resolve("number1_ptr", tank, number1_ptr);
    resolve("number2_ptr", tank, number2_ptr);


    ASSERT_EQ(resolve<int*>("number1_ptr", tank), number1_ptr);
    ASSERT_EQ(resolve<int*>("number2_ptr", tank), number2_ptr);

    *number1_ptr +=2;
    ASSERT_EQ(*std::any_cast<int*>(tank["number1_ptr"]), 102);
}