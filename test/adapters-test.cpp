#include "gtest/gtest.h"

#include "IUObject.h"
#include "interfaces.h"
#include "resolve.h"

TEST(adapters, normal)
{
    UObject *tank = new UObject();
    FuelableAdapter<int> *fuelable = new FuelableAdapter<int>(*tank);
    fuelable->setFuelLevel(100);

    ASSERT_EQ(fuelable->getFuelLevel(), 100);

    fuelable->setFuelLevel(90);

    ASSERT_EQ(fuelable->getFuelLevel(), 90);
}