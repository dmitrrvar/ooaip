#pragma once

#include "interfaces.h"
#include <vector>
#include <queue>

class CommandException {
};

class Command {
public:
    virtual void Execute() = 0;
    virtual ~Command()= default;
};


class MoveCommand : public Command {
    Movable<std::vector<int>> &m_movable;
    const std::vector<int> &m_value;
public:
    MoveCommand(Movable<std::vector<int>> &movable, const std::vector<int> &value) : m_movable(movable), m_value(value) {}

    void Execute() {
        std::vector<int> temp;
        temp = m_movable.getPosition();
        if (temp.size() != m_value.size()) {
            throw CommandException();
        }
        temp[0] += m_value[0];
        temp[1] += m_value[1];
        m_movable.setPosition(temp);
    }
};

class MacroCommand : public Command{
    std::queue<Command*>& cmds_;
public:
    MacroCommand(std::queue<Command*>& cmds): cmds_(cmds){}

    void Execute(){
        for(int i = 0; i<cmds_.size(); i++){
            try{
                cmds_.front()->Execute();
                cmds_.pop();
            }catch(std::exception) {
                throw CommandException();
            }

        }
    }
};

class EmptyFuel{};

class IsEmptyFuelCommand : public Command{
    Fuelable<int>  &m_fuelable;
public:
    IsEmptyFuelCommand(Fuelable<int>  &fuelable) : m_fuelable(fuelable) {}
    ~IsEmptyFuelCommand(){}
    void Execute() {
        if(m_fuelable.getFuelLevel() == 0) {
            throw EmptyFuel();
        }
    }
};

class BurnFuelCommand : public Command{
    private:
    Fuelable<int>  &m_fuelable;
    int m_to_burn;
    public:
    BurnFuelCommand(Fuelable<int>  &fuelable, int to_burn) : m_fuelable(fuelable), m_to_burn(to_burn) {}
    ~BurnFuelCommand(){}
    void Execute()
    {
        int current_level = m_fuelable.getFuelLevel();
        m_fuelable.setFuelLevel(current_level - m_to_burn);
    }
};

class GameCommands {
    std::queue<Command*> cmds_;
    public:
    void AddCommand(Command* cmd){
        cmds_.push(cmd);
    }
    void Execute(){
        cmds_.front()->Execute();
        cmds_.pop();
    }
};

class RepeatCommand : public Command{
    GameCommands& gcmds_;
    Command* cmd_;
    int times_ = 1;
    public:
    RepeatCommand(GameCommands& gcmds, Command* cmd): gcmds_(gcmds), cmd_(cmd){}
    RepeatCommand(GameCommands& gcmds, Command* cmd, int times): gcmds_(gcmds), cmd_(cmd), times_(times){}
    void Execute(){
        if(times_ != 1){
            gcmds_.AddCommand(cmd_);
            RepeatCommand* repeat = new RepeatCommand(gcmds_, cmd_, times_ - 1);
            gcmds_.AddCommand(repeat);
        }
        else{
            gcmds_.AddCommand(cmd_);
        }
    }
};
